package praksa.api.service;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import praksa.api.beans.MessageBean;
import praksa.api.beans.UserBean;

public class UserService extends Service {
	
	public UserService(String url, String usr, String psw) throws SQLException {
		super(url, usr, psw);
	}
	
public List<UserBean> createUser(String name, String mail, String password, String phone, Integer companyID) {
		
		List<UserBean> ub = new ArrayList<UserBean>();
		
		String sql = "INSERT INTO users (name_and_surname, user_mail, user_password, user_phone, companyID)" + 
				"values (?, ?, ?, ?, ?);";
		try {
			PreparedStatement ps = conn.prepareStatement(sql);
			
			ps.setString(1, name);
			ps.setString(2, mail);
			ps.setString(3, password);
			ps.setString(4, phone);
			ps.setInt(5, companyID);
			
			boolean b = ps.execute();
			ResultSet rs = ps.getResultSet();
			while(rs.next()) {
				UserBean usb = new UserBean();
				usb.setName(rs.getString(1));
				usb.setMail(rs.getString(2));
				usb.setPassword(rs.getString(3));
				usb.setPhone(rs.getString(4));
				usb.setCompanyID(rs.getInt(5));
				
				
				ub.add(usb);
			}	
			return ub;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ub;
		
}

public static void main (String[] args) {
	
	try {
		UserService us = new UserService("jdbc:mysql://localhost/Messenger", "root", "mzsql");
		us.createConnection(true);
		

		List<UserBean> ub = us.createUser("Zdenko Savic", "zdenko@mail.rs", "zdenkopass", "34344", 1);
		for(UserBean usb:ub) {
			System.out.println(usb);
		}}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}}
	
}
