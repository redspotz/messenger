package praksa.api.beans;

import java.sql.Date;

public class UserBean {
	
	private Integer userID;
	private String name;
	private String mail;
	private String password;
	private String phone;
	private Integer companyID;
	private Date createTime;
	private Date updateTime;
	private String state;
	
	public Integer getUserID() {
		return userID;
	}

	public void setUserID(Integer userID) {
		this.userID = userID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Integer getCompanyID() {
		return companyID;
	}

	public void setCompanyID(Integer companyID) {
		this.companyID = companyID;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Date getCreateTime() {
		return createTime;
	}

	@Override
	public String toString() {
		return "UserBean [userID=" + userID + ", name=" + name + ", mail=" + mail + ", password=" + password
				+ ", phone=" + phone + ", companyID=" + companyID + ", createTime=" + createTime + ", updateTime="
				+ updateTime + ", state=" + state + "]";
	}

}
