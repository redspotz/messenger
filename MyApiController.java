package praksa.api.controller;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import praksa.api.bean.MessageBean;
import praksa.api.service.MessageService;

@RestController
@RequestMapping("/api")
public class MyApiController {	
	
	@Value("${spring.datasource.url}")
	private String url;
	@Value("${spring.datasource.username}")
	private String usr;
	@Value("${spring.datasource.password}")
	private String psw;
	
	@GetMapping("/messages/{ownerId}")
	public ResponseEntity<List<MessageBean>> showMessages(@PathVariable Integer ownerId) {
		List<MessageBean> lmb = null;
		ResponseEntity<List<MessageBean>> re = null;
		
		try {
			MessageService ms = new MessageService(url, usr, psw);
			ms.createConnection(true);
			lmb = ms.showMessages(ownerId);
			
			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.set("Access-Control-Allow-Origin", "*");
			return new ResponseEntity<List<MessageBean>>(lmb, responseHeaders, HttpStatus.OK);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return re;
	}
}
